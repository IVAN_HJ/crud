<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Smartsystem_controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('smartsystem_model');
    }

    public function index() {
        $this->load->view('smartsystem_view_inicio');
    }

   public function guardar() {
        if ($_POST) {
            $this->smartsystem_model->guardar($_POST);
        }
        $this->load->view('smartsystem_view_inicio');
    }
    public function borrar(){
        $this->smartsystem_model->borrar($_GET);
        $this->load->view('smartsystem_view_inicio');
    }

}
