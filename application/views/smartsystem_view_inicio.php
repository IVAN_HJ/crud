<?php
$CI = & get_instance();
if ($this->uri->segment(3) == 0) {
    $dato[0]['id'] = "";
    $dato[0]['titulo'] = "";
    $dato[0]['resumen'] = "";
    $dato[0]['anio'] = "";
    $dato[0]['pais'] = "";
    $dato[0]['protagonistas'] = "";
} else {
    $CI->db->where('id', $this->uri->segment(3));
    $dato = $CI->db->get('peliculas')->result_array();
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <!--boostrap bar-->
        <meta charset="UTF-8">
        <title>Demo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>


        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>plugins/fontawesome-free/css/all.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>plugins/datatables-bs4/css/dataTables.bootstrap4.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url() . "assets/"; ?>dist/css/adminlte.min.css">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">



        <!-- -->
        
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

        <!-- Optional theme -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    </head>
    <body>


        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <!-- Brand/logo -->


            <!-- Links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" >Peliculas</a>
                </li>

            </ul>
        </nav>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">Agregar Pelicula</div>
                        <div class="panel-body">
                            <form action="<?= base_url('smartsystem_controller/guardar') ?>" method="post">
                                <!-- -->
                                <div class="col-md-12 form-group input-group">
                                    <!--<label for="" class="input-group-addon">Id</label>-->
                                    <input required type="hidden" name="id" class="form-control" value="<?= $dato[0]['id'] ?>">  
                                </div>
                                <!-- -->
                                <div class="col-md-12 form-group input-group">
                                    <label for="" class="input-group-addon">Titulo</label>
                                    <input required type="text" name="titulo" class="form-control" value="<?= $dato[0]['titulo'] ?>">  
                                </div>
                                <div class="col-md-12 form-group input-group">
                                    <label for="" class="input-group-addon">Resumen</label>
                                    <input required type="text" name="resumen" class="form-control" value="<?= $dato[0]['resumen'] ?>">  
                                </div>
                                <div class="col-md-12 form-group input-group">
                                    <label for="" class="input-group-addon">Año</label>
                                    <input required type="text" name="anio" class="form-control" value="<?= $dato[0]['anio'] ?>">  
                                </div>
                                <div class="col-md-12 form-group input-group">
                                    <label for="" class="input-group-addon">Pais</label>
                                    <input required type="text" name="pais" class="form-control" value="<?= $dato[0]['pais'] ?>">  
                                </div>
                                <div class="col-md-12 form-group input-group">
                                    <label for="" class="input-group-addon">Protagonistas</label>
                                    <textarea required name="protagonistas" class="form-control"><?= $dato[0]['protagonistas'] ?></textarea>  
                                </div>
                                <div class="col-md-12">
                                    <a href="<?= base_url("smartsystem_controller/guardar/0") ?>" class="btn btn-primary">Nuevo registro</a>
                                    <button type="submit" class="btn btn-success">Guardar datos</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="panel panel-default overflow-auto">
                        <div class="panel-heading">peliculas</div>
                        <div class="panel-body">
                            <table id="example1"class="table table-bordered table-striped" >
                                <thead>
                                <th>Id</th>
                                <th>Titulo</th>
                                <th>Resumen</th>
                                <th>Año</th>
                                <th>Pais</th>
                                <th>Protagonistas</th>
                                <th></th>

                                </thead>
                                <tbody>
                                    <?php
//                                    $CI = & get_instance();
                                    $datos = $CI->db->get('peliculas')->result_array();

                                    foreach ($datos as $dato) {

                                        $ruta_editar = base_url("smartsystem_controller/guardar/{$dato['id']}");
                                        $ruta_borrar = base_url("smartsystem_controller/borrar?borrar={$dato['id']}");
                                        echo "<tr>
                                        <td>{$dato['id']}</td>
                                        <td>{$dato['titulo']}</td>
                                        <td>{$dato['resumen']}</td>
                                        <td>{$dato['anio']}</td>
                                        <td>{$dato['pais']}</td>
                                        <td>{$dato['protagonistas']}</td>
                                        <td>
                                        <a href='{$ruta_editar}' class='btn btn-info glyphicon glyphicon-pencil'></a>
                                        <a href='{$ruta_borrar}' onclick='return confirm(\"Desea borrar el registro\")' class='btn btn-danger glyphicon glyphicon-remove'></a>
                                        </td>
                                        </tr>
                                        ";
                                    }
                                    ?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- jQuery -->
        <script src="<?php echo base_url() . "assets/"; ?>plugins/jquery/jquery.min.js"></script>
        <!-- Bootstrap 4 -->
        <script src="<?php echo base_url() . "assets/"; ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url() . "assets/"; ?>plugins/datatables/jquery.dataTables.js"></script>
        <script src="<?php echo base_url() . "assets/"; ?>plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>

        <!-- page script -->
        <script>
            $(function () {
                $("#example1").DataTable();
                $('#example2').DataTable({
                    "paging": true,
                    "lengthChange": false,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": false,
                });
            });
        </script>
    </body>      
</html>