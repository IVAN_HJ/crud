<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Smartsystem_model extends CI_Model {

    public $variable;

    public function __construct() {
        parent::__construct();
    }

    function guardar($post) {
        $datos = array();
        $datos['id']               =$post['id'];
        $datos['titulo'] = $post['titulo'];
        $datos['resumen'] = $post['resumen'];
        $datos['anio'] = $post['anio'];
        $datos['pais'] = $post['pais'];
        $datos['protagonistas'] = $post['protagonistas'];
        
        if ($datos['id'] > 0) {
            $this->db->where('id', $datos['id']);
            $this->db->update('peliculas', $datos);
            $ruta = base_url('smartsystem_controller');
            echo "<script>
             alert('Datos modificados correctamente.');
            window.location = '{$ruta}';
                </script>";die();
        } else {
            $this->db->insert('peliculas', $datos);
            $ruta = base_url('smartsystem_controller');
            echo "<script>
             alert('Datos guardados correctamente.');
            window.location = '{$ruta}';
                </script>";
        }

    }
    
     function borrar($get) {

            $this->db->where('id', $get['borrar']);
            $this->db->delete('peliculas');
        }

}
